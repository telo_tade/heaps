package randomized_test

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/heaps/randomized"
)

type IntKey int

func (i IntKey) Less(k randomized.Key) bool {
	return i < k.(IntKey)
}

func (i IntKey) Eq(k randomized.Key) bool {
	return i == k.(IntKey)
}

func (i IntKey) String() string {
	return strconv.Itoa(int(i))
}

func TestRandomizedHeapPush(t *testing.T) {
	heap := randomized.New()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3))
	heap.Push(IntKey(2))
	heap.Push(IntKey(1))

	assert.Equal(t, 3, heap.Size(), "Size  should be 3")

	actualValue := heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(3), actualValue, "actualValue should be 3")

	actualValue = heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(2), actualValue, "actualValue should be 2")

	actualValue = heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(1), actualValue, "actualValue should be 1")

	assert.Equal(t, 0, heap.Size(), "size should be 0")
}

func TestRandomizedHeapPushBulk(t *testing.T) {
	heap := randomized.New()

	heap.Push(IntKey(15), IntKey(20), IntKey(3), IntKey(1), IntKey(2))

	assert.Equal(t, 5, heap.Size(), "Size  should be 5")

	actualValue := heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(20), actualValue, "actualValue should be 20")

	actualValue = heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(15), actualValue, "actualValue should be 15")

	actualValue = heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(3), actualValue, "actualValue should be 3")

	assert.Equal(t, 2, heap.Size(), "size should be 2")
}

func TestRandomizedHeapPop(t *testing.T) {
	heap := randomized.New()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3))
	heap.Push(IntKey(2))
	heap.Push(IntKey(1))
	heap.Pop()

	actualValue := heap.Peek()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(2), actualValue, "actualValue should be 2")

	actualValue = heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(2), actualValue, "actualValue should be 2")

	actualValue = heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(1), actualValue, "actualValue should be 1")

	actualValue = heap.Pop()
	assert.Nil(t, actualValue, "actualValue should be nil")
	assert.Nil(t, actualValue, "actualValue should be nil")
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	assert.Equal(t, 0, len(heap.Values()), "len(heap.Values()) should be 0")
}

func TestRandomizedHeapRandom(t *testing.T) {
	heap := randomized.New()

	for i := 0; i < 1000; i++ {
		r, _ := rand.Int(rand.Reader, big.NewInt(30))
		heap.Push(IntKey(r.Int64()))
	}

	prev := heap.Pop()
	assert.NotNil(t, prev, "prev should not be nil")

	for heap.Size() != 0 {
		curr := heap.Pop()
		assert.NotNil(t, curr, "curr should not be nil")

		assert.False(t, prev.Less(curr),
			"Heap property invalidated. prev: %v current: %v", prev, curr)

		prev = curr
	}
}

func TestString(t *testing.T) {
	heap := randomized.New()

	heap.Push(IntKey(3), IntKey(21), IntKey(14))

	actual := heap.String()
	expected := "RandomizedHeap:\n(1, 3), (0, 21), (1, 14)"

	assert.Equal(t, expected, actual)
}

func TestValues(t *testing.T) {
	heap := randomized.New()

	heap.Push(IntKey(3), IntKey(21), IntKey(14))

	actual := fmt.Sprintf("%v", heap.Values())
	expected := "[3 21 14]"

	assert.Equal(t, expected, actual)
}

func TestSize(t *testing.T) {
	heap := randomized.New()
	assert.Equal(t, 0, heap.Size())

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	assert.Equal(t, 5, heap.Size())
}

func TestPeek(t *testing.T) {
	heap := randomized.New()

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	actual := heap.Peek()
	assert.Equal(t, IntKey(21), actual)
}

func TestClear(t *testing.T) {
	heap := randomized.New()
	heap.Clear()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3), IntKey(21), IntKey(14))
	assert.Equal(t, "[3 21 14]", fmt.Sprintf("%v", heap.Values()))

	heap.Clear()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3), IntKey(21), IntKey(14))
	assert.Equal(t, "[3 21 14]", fmt.Sprintf("%v", heap.Values()))
}
