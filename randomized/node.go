package randomized

// Node models a node in the heap (implemented as a binary tree).
type Node struct {
	value               Key
	left, right, parent *Node
}

// bubbleUp swaps the values of node and its parent, as long as node is greater.
func (n *Node) bubbleUp() {
	if n == nil {
		return
	}

	for n.parent != nil && n.parent.value.Less(n.value) {
		n.parent.value, n.value = n.value, n.parent.value

		n = n.parent
	}
}

// bubbleDown swaps the value of node with one of its children, as long as node is smaller.
func (n *Node) bubbleDown() {
	if n == nil || (n.left == nil && n.right == nil) {
		return
	}

	for {
		switch {
		case n.left != nil && n.right != nil:
			bigger := n.left
			if n.left.value.Less(n.right.value) {
				bigger = n.right
			}

			if bigger.value.Less(n.value) {
				return
			}

			n.value, bigger.value = bigger.value, n.value
			n = bigger
		case n.left != nil && n.value.Less(n.left.value):
			n.value, n.left.value = n.left.value, n.value
			n = n.left
		case n.right != nil && n.value.Less(n.right.value):
			n.value, n.right.value = n.right.value, n.value
			n = n.right
		default:
			return
		}
	}
}

// Traverse performs inorder traversal (left, root, right).
func Traverse(n *Node, f func(*Node, int), level int) {
	if n == nil {
		return
	}

	Traverse(n.left, f, 1+level)

	f(n, level)

	Traverse(n.right, f, 1+level)
}
