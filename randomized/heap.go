package randomized

// Heap interface models the operations offered by a heap data structure.
type Heap interface {
	Add(val Key)
	Push(values ...Key)
	Pop() Key
	Clear()
	String() string
	Values() []Key
	Size() int
	Peek() Key
}
