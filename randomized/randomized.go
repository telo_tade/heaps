package randomized

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"strings"
)

// Randomized implements the Heap interface.
type Randomized struct {
	root          *Node
	elementsCount int
}

// New instantiates a new empty heap tree.
func New() Heap {
	return &Randomized{root: nil, elementsCount: 0}
}

// Add adds a value onto the heap and bubbles it up accordingly.
func (h *Randomized) Add(val Key) {
	if val == nil {
		return
	}

	newNode := Node{value: val, left: nil, right: nil, parent: nil}

	if h.elementsCount == 0 {
		h.root = &newNode
		h.elementsCount = 1

		return
	}

	h.elementsCount++

	parent := h.findRandomHalfNode()
	if parent.left == nil {
		parent.left = &newNode
	} else {
		parent.right = &newNode
	}

	newNode.parent = parent
	newNode.bubbleUp()
}

// findRandomHalfNode returns a random half node (== node having only one child).
func (h *Randomized) findRandomHalfNode() *Node {
	current := h.root

	for current.left != nil && current.right != nil {
		if randomBool() {
			current = current.left
		} else {
			current = current.right
		}
	}

	return current
}

// findLeaf returns a leaf node: namely a node with both nil children.
func (h *Randomized) findRandomLeaf() *Node {
	current := h.root

	for current.left != nil || current.right != nil {
		switch {
		case current.left != nil && current.right != nil:
			if randomBool() {
				current = current.left
			} else {
				current = current.right
			}
		case current.left == nil:
			current = current.right
		case current.right == nil:
			current = current.left
		}
	}

	return current
}

func randomBool() bool {
	r, _ := rand.Int(rand.Reader, big.NewInt(100))

	return r.Int64()%2 == 0
}

// Push adds a value onto the heap and bubbles it up accordingly.
func (h *Randomized) Push(values ...Key) {
	for i := range values {
		h.Add(values[i])
	}
}

// Pop removes top element on heap and returns it, or nil if heap is empty.
// Second return parameter is true, unless the heap was empty and there was nothing to pop.
func (h *Randomized) Pop() Key {
	if h.elementsCount == 0 {
		return nil
	}

	h.elementsCount--
	result := h.root.value

	if h.root.left == nil {
		h.root = h.root.right

		return result
	}

	if h.root.right == nil {
		h.root = h.root.left

		return result
	}

	leaf := h.findRandomLeaf()
	if leaf.parent.left == leaf {
		leaf.parent.left = nil
	} else {
		leaf.parent.right = nil
	}

	h.root.value = leaf.value
	h.root.bubbleDown()

	return result
}

// Clear removes all elements from the heap.
func (h *Randomized) Clear() {
	h.root = nil
	h.elementsCount = 0
}

// String returns a string representation of container.
func (h *Randomized) String() string {
	values := make([]string, 0, h.elementsCount)

	Traverse(h.root, func(n *Node, level int) {
		values = append(values, fmt.Sprintf("(%d, %s)", level, n.value))
	}, 0)

	return "RandomizedHeap:\n" + strings.Join(values, ", ")
}

// Values returns all elements in the heap.
func (h *Randomized) Values() []Key {
	result := make([]Key, 0, h.elementsCount)

	Traverse(h.root, func(n *Node, level int) {
		result = append(result, n.value)
	}, 0)

	return result
}

// Size returns number of elements within the heap.
func (h *Randomized) Size() int {
	return h.elementsCount
}

// Peek returns the biggest element without removing it, (nil, false) if heap is empty.
func (h *Randomized) Peek() Key {
	if h.elementsCount == 0 {
		return nil
	}

	return h.root.value
}
