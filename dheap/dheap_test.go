package dheap_test

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/heaps/dheap"
)

type IntKey int

func (i IntKey) Less(k dheap.Key) bool {
	return i < k.(IntKey)
}

func (i IntKey) Eq(k dheap.Key) bool {
	return i == k.(IntKey)
}

func (i IntKey) String() string {
	return strconv.Itoa(int(i))
}

func TestDHeapPush(t *testing.T) {
	heap := dheap.New(12)
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	SIZE := 50000
	for i := 0; i <= SIZE; i++ {
		heap.Push(IntKey(i))
	}

	for i := SIZE; i >= 0; i-- {
		val := heap.Pop()
		assert.NotNil(t, val, "%v: val should not be nil", i)
		assert.True(t, val.Eq(IntKey(i)), "wrong pop %v - %d", val, i)

		assert.Equal(t, i, heap.Size(), "wrong length  %d - %d", heap.Size(), i)
	}
}

func TestDHeapPushBulk(t *testing.T) {
	heap := dheap.New(7)

	heap.Push(IntKey(15), IntKey(20), IntKey(3), IntKey(1), IntKey(2))

	assert.Equal(t, "[20 15 3 1 2]", fmt.Sprintf("%v", heap.Values()),
		"Got %v expected %v", fmt.Sprintf("%v", heap.Values()), "[20 15 3 1 2]")

	actualValue := heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(20), actualValue, "actualValue should be 20")
}

func TestDHeapPop(t *testing.T) {
	heap := dheap.New(8)
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3))
	heap.Push(IntKey(2))
	heap.Push(IntKey(1))
	heap.Pop()

	actualValue := heap.Peek()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(2), actualValue, "actualValue should be 2")

	actualValue = heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(2), actualValue, "actualValue should be 2")

	actualValue = heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(1), actualValue, "actualValue should be 1")

	actualValue = heap.Pop()
	assert.Nil(t, actualValue, "actualValue should be nil")
	assert.Nil(t, actualValue, "actualValue should be nil")

	assert.Equal(t, 0, heap.Size(), "heap should be empty")
	assert.Equal(t, 0, len(heap.Values()), "len(heap.Values()) should be 0")
}

func TestDHeapRandom(t *testing.T) {
	heap := dheap.New(4)

	for i := 0; i < 1000000; i++ {
		r, _ := rand.Int(rand.Reader, big.NewInt(30))
		heap.Push(IntKey(r.Int64()))
	}

	prev := heap.Pop()
	assert.NotNil(t, prev, "prev should not be nil")

	for heap.Size() != 0 {
		curr := heap.Pop()
		assert.NotNil(t, curr, "curr should not be nil")

		assert.False(t, prev.Less(curr),
			"Heap property invalidated. prev: %v current: %v", prev, curr)

		prev = curr
	}
}

func TestParent(t *testing.T) {
	heap := dheap.New(5)

	assert.Equal(t, 0, heap.Parent(-5), "Wrong parent value")
	assert.Equal(t, 2, heap.Parent(15), "Wrong parent value")
	assert.Equal(t, 3, heap.Parent(16), "Wrong parent value")
}

func TestLeft(t *testing.T) {
	heap := dheap.New(6)

	assert.Equal(t, 0, heap.Left(-5), "Wrong left value")
	assert.Equal(t, 31, heap.Left(5), "Wrong left value")
	assert.Equal(t, 37, heap.Left(6), "Wrong left value")
}

func TestString(t *testing.T) {
	heap := dheap.New(3)

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	actual := heap.String()
	expected := "3-DHeap:\n21, 18, 14, 7, 3"

	assert.Equal(t, expected, actual)
}

func TestValues(t *testing.T) {
	heap := dheap.New(3)

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	actual := fmt.Sprintf("%v", heap.Values())
	expected := "[21 18 14 7 3]"

	assert.Equal(t, expected, actual)
}

func TestSize(t *testing.T) {
	heap := dheap.New(4)
	assert.Equal(t, 0, heap.Size())

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	assert.Equal(t, 5, heap.Size())
}

func TestPeek(t *testing.T) {
	heap := dheap.New(4)

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	actual := heap.Peek()
	assert.Equal(t, IntKey(21), actual)
}

func TestClear(t *testing.T) {
	heap := dheap.New(4)
	heap.Clear()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))
	assert.Equal(t, "[21 3 14 7 18]", fmt.Sprintf("%v", heap.Values()))

	heap.Clear()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))
	assert.Equal(t, "[21 3 14 7 18]", fmt.Sprintf("%v", heap.Values()))
}
