package dheap

import (
	"fmt"
	"strconv"
	"strings"
)

// DHeap holds elements in an array-list.
type DHeap struct {
	list []Key
	d    int
}

// New instantiates a new empty heap tree.
func New(dimensions int) Heap {
	return &DHeap{list: []Key{}, d: dimensions}
}

// Parent returns the parent node's index.
func (h *DHeap) Parent(index int) int {
	if index <= 0 {
		return 0
	}

	return (index - 1) / h.d
}

// Left returns the left most child's index.
func (h *DHeap) Left(index int) int {
	if index < 0 {
		return 0
	}

	return index*h.d + 1
}

// Push adds a value onto the heap and bubbles it up accordingly.
func (h *DHeap) Push(values ...Key) {
	if len(values) == 1 {
		h.Add(values[0])

		return
	}

	filtered := h.filterNils(values)
	h.list = append(h.list, filtered...)

	for i := len(h.list)/h.d + 1; i >= 0; i-- {
		h.bubbleDownIndex(i)
	}
}

func (h *DHeap) filterNils(input []Key) []Key {
	result := make([]Key, 0, len(input))

	for i := 0; i < len(input); i++ {
		if input[i] != nil {
			result = append(result, input[i])
		}
	}

	return result
}

// Add add a new value into this heap.
func (h *DHeap) Add(val Key) {
	if val == nil {
		return
	}

	h.list = append(h.list, val)

	h.bubbleUp()
}

// Pop removes top element on heap and returns it, or nil if heap is empty.
// Second return parameter is true, unless the heap was empty and there was nothing to pop.
func (h *DHeap) Pop() Key {
	if len(h.list) == 0 {
		return nil
	}

	result := h.list[0]
	lastIndex := len(h.list) - 1

	h.list[0], h.list[lastIndex] = h.list[lastIndex], h.list[0]
	h.list = h.list[0:lastIndex]

	h.bubbleDown()

	return result
}

// Clear removes all elements from the heap.
func (h *DHeap) Clear() {
	h.list = []Key{}
}

// String returns a string representation of container.
func (h *DHeap) String() string {
	values := make([]string, 0, h.Size())

	for _, value := range h.list {
		values = append(values, fmt.Sprintf("%s", value))
	}

	return strconv.Itoa(h.d) + "-DHeap:\n" + strings.Join(values, ", ")
}

// Performs the "bubble down" operation. This is to place the element that is at the root
// of the heap in its correct place so that the heap maintains the min/max-heap order property.
func (h *DHeap) bubbleDown() {
	h.bubbleDownIndex(0)
}

func (h *DHeap) findLargestChild(index int) (int, bool) {
	if h == nil || index >= len(h.list) || h.d <= 0 {
		return 0, false
	}

	child := h.Left(index)
	if child >= len(h.list) {
		return 0, false
	}

	result := child

	for i := 0; i < h.d && i+child < len(h.list); i++ {
		if h.list[result].Less(h.list[child+i]) {
			result = child + i
		}
	}

	return result, true
}

// Performs the "bubble down" operation. This is to place the element that is at the index
// of the heap in its correct place so that the heap maintains the min/max-heap order property.
func (h *DHeap) bubbleDownIndex(index int) {
	size := len(h.list)

	for index < size {
		largestChild, ok := h.findLargestChild(index)

		if !ok {
			return
		}

		if h.list[index].Less(h.list[largestChild]) {
			h.list[index], h.list[largestChild] = h.list[largestChild], h.list[index]
			index = largestChild
		} else {
			index = size
		}
	}
}

// Performs the "bubble up" operation. This is to place a newly inserted
// element (i.e. last element in the list) in its correct place so that
// the heap maintains the min/max-heap order property.
func (h *DHeap) bubbleUp() {
	index := len(h.list) - 1

	for parentIndex := h.Parent(index); index > 0; parentIndex = h.Parent(index) {
		indexValue := h.list[index]
		parentValue := h.list[parentIndex]

		if indexValue.Less(parentValue) {
			break
		}

		h.list[index], h.list[parentIndex] = h.list[parentIndex], h.list[index]
		index = parentIndex
	}
}

// Values returns all elements in the heap.
func (h *DHeap) Values() []Key {
	return h.list
}

// Size returns number of elements within this heap.
func (h *DHeap) Size() int {
	return len(h.list)
}

// Peek returns the biggest element without removing it, (nil, false) if heap is empty.
func (h *DHeap) Peek() Key {
	if len(h.list) == 0 {
		return nil
	}

	return h.list[0]
}
