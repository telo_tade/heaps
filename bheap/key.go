package bheap

// Key models an element of the heap.
type Key interface {
	Less(Key) bool
	Eq(Key) bool
}
