package bheap_test

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/heaps/bheap"
)

// IntKey implements a key.
type IntKey int

// Less method for IntKey.
func (i IntKey) Less(k bheap.Key) bool {
	return i < k.(IntKey)
}

// Eq method for IntKey.
func (i IntKey) Eq(k bheap.Key) bool {
	return i == k.(IntKey)
}

func (i IntKey) String() string {
	return strconv.Itoa(int(i))
}

func TestBHeapPushConsecutive(t *testing.T) {
	heap := bheap.New()

	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	for i := 0; i < 100; i++ {
		heap.Push(IntKey(i))
	}

	prev := heap.Pop()
	assert.NotNil(t, prev, "prev should not be nil")

	for heap.Size() != 0 {
		curr := heap.Pop()
		assert.NotNil(t, curr, "curr should not be nil")

		assert.False(t, prev.Less(curr), "Prev %v less than curr %v", prev, curr)

		prev = curr
	}
}

func TestBHeapPush(t *testing.T) {
	heap := bheap.New()

	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(40), IntKey(5), IntKey(7), IntKey(23))

	prev := heap.Pop()
	assert.NotNil(t, prev, "prev should not be nil")

	for heap.Size() != 0 {
		curr := heap.Pop()
		assert.NotNil(t, curr, "curr should not be nil")

		assert.False(t, prev.Less(curr), "Prev %v less than curr %v", prev, curr)

		prev = curr
	}
}

func TestBHeapPushBulk(t *testing.T) {
	heap := bheap.New()

	heap.Push(IntKey(15), IntKey(20), IntKey(3), IntKey(1), IntKey(2))

	assert.Equal(t, "[20 15 3 1 2]", fmt.Sprintf("%v", heap.Values()),
		"Got %v expected %v", fmt.Sprintf("%v", heap.Values()), "[20 15 3 1 2]")

	actualValue := heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(20), actualValue, "actualValue should be 20")
}

func TestBHeapPop(t *testing.T) {
	heap := bheap.New()

	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3))
	heap.Push(IntKey(2))
	heap.Push(IntKey(1))
	heap.Pop()

	actualValue := heap.Peek()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(2), actualValue, "actualValue should be 2")

	actualValue = heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(2), actualValue, "actualValue should be 2")

	actualValue = heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(1), actualValue, "actualValue should be 1")

	actualValue = heap.Pop()
	assert.Nil(t, actualValue, "actualValue should be nil")
	assert.Nil(t, actualValue, "actualValue should be nil")

	assert.Equal(t, 0, heap.Size(), "heap should be empty")
	assert.Equal(t, 0, len(heap.Values()), "len(heap.Values()) should be 0")
}

func TestBHeapRandom(t *testing.T) {
	heap := bheap.New()

	for i := 0; i < 1000; i++ {
		r, _ := rand.Int(rand.Reader, big.NewInt(30))
		heap.Push(IntKey(r.Int64()))
	}

	prev := heap.Pop()
	assert.NotNil(t, prev, "prev should not be nil")

	for heap.Size() != 0 {
		curr := heap.Pop()
		assert.NotNil(t, curr, "curr should not be nil")

		assert.False(t, prev.Less(curr),
			"Heap property invalidated. prev: %v current: %v", prev, curr)

		prev = curr
	}
}

func TestParent(t *testing.T) {
	heap := bheap.New()

	assert.Equal(t, 0, heap.Parent(-5), "Wrong parent value")
	assert.Equal(t, 16, heap.Parent(18), "Wrong parent value")
	assert.Equal(t, 17, heap.Parent(19), "Wrong parent value")
}

func TestChildren(t *testing.T) {
	heap := bheap.New()

	left, right := heap.Children(-5)
	assert.Equal(t, 0, left, "Wrong left value")
	assert.Equal(t, 0, right, "Wrong right value")

	left, right = heap.Children(12)
	assert.Equal(t, 40, left, "Wrong left value")
	assert.Equal(t, 41, right, "Wrong right value")
}

func TestString(t *testing.T) {
	heap := bheap.New()

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	actual := heap.String()
	expected := "BHeap:\n21, 18, 14, 7, 3"

	assert.Equal(t, expected, actual)
}

func TestValues(t *testing.T) {
	heap := bheap.New()

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	actual := fmt.Sprintf("%v", heap.Values())
	expected := "[21 18 14 7 3]"

	assert.Equal(t, expected, actual)
}

func TestSize(t *testing.T) {
	heap := bheap.New()
	assert.Equal(t, 0, heap.Size())

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	assert.Equal(t, 5, heap.Size())
}

func TestPeek(t *testing.T) {
	heap := bheap.New()

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	actual := heap.Peek()
	assert.Equal(t, IntKey(21), actual)
}

func TestClear(t *testing.T) {
	heap := bheap.New()
	heap.Clear()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))
	assert.Equal(t, "[21 18 14 7 3]", fmt.Sprintf("%v", heap.Values()))

	heap.Clear()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))
	assert.Equal(t, "[21 18 14 7 3]", fmt.Sprintf("%v", heap.Values()))
}
