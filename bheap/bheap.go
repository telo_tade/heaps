package bheap

import (
	"fmt"
	"strings"
)

// BHeap holds elements in an array-list.
type BHeap struct {
	list []Key
}

// New instantiates a new empty heap tree.
func New() Heap {
	return &BHeap{list: []Key{nil}}
}

// Add inserts a new val into the heap.
func (h *BHeap) Add(val Key) {
	if val == nil {
		return
	}

	h.list = append(h.list, val)

	h.bubbleUp()
}

// Push adds a value onto the heap and bubbles it up accordingly.
func (h *BHeap) Push(values ...Key) {
	if len(values) == 1 {
		h.Add(values[0])

		return
	}

	filtered := h.filterNils(values)
	h.list = append(h.list, filtered...)

	for i := len(h.list) - 1; i >= 0; i-- {
		h.bubbleDownIndex(i)
	}
}

func (h *BHeap) filterNils(input []Key) []Key {
	result := make([]Key, 0, len(input))

	for i := 0; i < len(input); i++ {
		if input[i] != nil {
			result = append(result, input[i])
		}
	}

	return result
}

// Pop removes top element on heap and returns it, or nil if heap is empty.
// Second return parameter is true, unless the heap was empty and there was nothing to pop.
func (h *BHeap) Pop() Key {
	if len(h.list) <= 1 {
		return nil
	}

	result := h.list[1]
	lastIndex := len(h.list) - 1

	h.list[1], h.list[lastIndex] = h.list[lastIndex], h.list[1]
	h.list = h.list[0:lastIndex]

	h.bubbleDown()

	return result
}

// Empty returns true if heap does not contain any elements.
func (h *BHeap) Empty() bool {
	return len(h.list) <= 1
}

// Clear removes all elements from the heap.
func (h *BHeap) Clear() {
	h.list = []Key{nil}
}

// String returns a string representation of container.
func (h *BHeap) String() string {
	values := make([]string, 0, h.Size())

	for _, value := range h.list[1:] {
		values = append(values, fmt.Sprintf("%s", value))
	}

	return "BHeap:\n" + strings.Join(values, ", ")
}

// Performs the "bubble down" operation. This is to place the element that is at the root
// of the heap in its correct place so that the heap maintains the min/max-heap order property.
func (h *BHeap) bubbleDown() {
	h.bubbleDownIndex(1)
}

/*
Children returns the left and right children indexes
- first element is null
- each 8 element form one page
- i-th node belongs to i / 8 page
- pages from 0 to ...
- page k = position (8*k, 8*k + 7)
- node i on page k (== i/8) has children:
	(i + 2) for i%8 == 0 or 1
	(i + 2), (i + 3) for i%8 == 2
	(i + 3), (i + 4) for i%8 == 3
	i%8 ==  4 - (0, 1) from 1st child page
	i%8 ==  5 - (0, 1) from 2nd child page
	i%8 ==  6 - (0, 1) from 3rd child page
	i%8 ==  7 - (0, 1) from 4th child page
- page k has 4 children: k*4 + 1, k*4 + 2, k*4 + 3, k*4 + 4.
*/
func (h *BHeap) Children(index int) (int, int) {
	if index < 0 {
		return 0, 0
	}

	switch index {
	case 0:
		return 0, 0
	case 1:
		return 2, 3
	case 2:
		return 4, 5
	case 3:
		return 6, 7
	case 4:
		return 8, 9
	case 5:
		return 16, 17
	case 6:
		return 24, 25
	case 7:
		return 32, 33
	}

	page := index / 8

	switch index % 8 {
	case 0, 1:
		return index + 2, index + 2
	case 2:
		return index + 2, index + 3
	case 3:
		return index + 3, index + 4
	case 4:
		return (page*4 + 1) * 8, (page*4+1)*8 + 1
	case 5:
		return (page*4 + 2) * 8, (page*4+2)*8 + 1
	case 6:
		return (page*4 + 3) * 8, (page*4+3)*8 + 1
	case 7:
		return (page*4 + 4) * 8, (page*4+4)*8 + 1
	default:
		return -1, -1
	}
}

// Performs the "bubble down" operation. This is to place the element that is at the index
// of the heap in its correct place so that the heap maintains the min/max-heap order property.
func (h *BHeap) bubbleDownIndex(index int) {
	size := len(h.list)
	if index == 0 || index >= size {
		return
	}

	for left, right := h.Children(index); left < size; left, right = h.Children(index) {
		largerIndex := left

		if left < size && right < size && h.list[left].Less(h.list[right]) {
			largerIndex = right
		}

		indexValue := h.list[index]
		smallerValue := h.list[largerIndex]

		if indexValue.Less(smallerValue) {
			h.list[index], h.list[largerIndex] = h.list[largerIndex], h.list[index]
		} else {
			break
		}

		index = largerIndex
	}
}

// Parent returns the parent's index.
func (h *BHeap) Parent(index int) int {
	if index < 0 {
		return 0
	}

	switch index {
	case 0, 1: // parent of 1 is 0 to end the push loop
		return 0
	case 2, 3:
		return 1
	case 4, 5:
		return 2
	case 6, 7:
		return 3
	}

	page := index / 8

	switch index % 8 {
	case 0, 1:
		return (page-1)/4*8 + 4 + (page-1)%4
	case 2:
		return index - 2
	case 3:
		return index - 2
	case 4:
		return index - 2
	case 5:
		return index - 3
	case 6:
		return index - 3
	case 7:
		return index - 4
	default:
		return -1
	}
}

// Performs the "bubble up" operation. This is to place a newly inserted
// element (i.e. last element in the list) in its correct place so that
// the heap maintains the min/max-heap order property.
func (h *BHeap) bubbleUp() {
	index := len(h.list) - 1

	for parentIndex := h.Parent(index); parentIndex > 0; parentIndex = h.Parent(index) {
		indexValue := h.list[index]

		if indexValue.Less(h.list[parentIndex]) {
			break
		}

		h.list[index], h.list[parentIndex] = h.list[parentIndex], h.list[index]
		index = parentIndex
	}
}

// Values returns all elements in this heap.
func (h *BHeap) Values() []Key {
	return h.list[1:]
}

// Size returns number of elements within the heap.
func (h *BHeap) Size() int {
	return len(h.list) - 1
}

// Peek returns the biggest element without removing it, (nil, false) if heap is empty.
func (h *BHeap) Peek() Key {
	if len(h.list) <= 1 {
		return nil
	}

	return h.list[1]
}
