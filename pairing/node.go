package pairing

// Node models a node of our pairing heap.
type Node struct {
	Value    Key
	Children []Node
}

// Merge combines the two nodes.
func (n *Node) Merge(other *Node) *Node {
	if n == nil {
		return other
	}

	if other == nil {
		return n
	}

	if n.Value.Less(other.Value) {
		n.Children = append(n.Children, *other)

		return n
	}

	other.Children = append(other.Children, *n)

	return other
}

// Traverse the heap rooted in n.
func Traverse(n *Node, f func(*Node, int), level int) {
	if n == nil {
		return
	}

	for i := 0; i < len(n.Children); i++ {
		Traverse(&n.Children[i], f, 1+level)
	}

	f(n, level)
}
