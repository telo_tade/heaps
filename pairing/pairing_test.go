package pairing_test

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/heaps/pairing"
)

// IntKey implement a key.
type IntKey int

// Less method for IntKey.
func (i IntKey) Less(k pairing.Key) bool {
	return i < k.(IntKey)
}

// Eq method for IntKey.
func (i IntKey) Eq(k pairing.Key) bool {
	return i == k.(IntKey)
}

func (i IntKey) String() string {
	return strconv.Itoa(int(i))
}

func TestPairingHeapPushConsecutive(t *testing.T) {
	heap := pairing.New()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	for i := 0; i < 10; i++ {
		heap.Push(IntKey(i))
	}

	for i := 0; i < 10; i++ {
		current := heap.Pop()

		assert.NotNil(t, current, "current should not be nil")
		assert.True(t, IntKey(i).Eq(current), "Got %v expected %v", current, i)
	}
}

func TestPairingHeapPush(t *testing.T) {
	heap := pairing.New()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(0))
	heap.Push(IntKey(1))
	heap.Push(IntKey(2))
	heap.Push(IntKey(3))
	heap.Push(IntKey(4))
	heap.Push(IntKey(5))
	heap.Push(IntKey(6))
	heap.Push(IntKey(7))
	heap.Push(IntKey(8))
	heap.Push(IntKey(1))

	prev := heap.Pop()
	assert.NotNil(t, prev, "prev should not be nil")

	for heap.Size() != 0 {
		curr := heap.Pop()

		assert.NotNil(t, curr, "curr should not be nil")
		assert.False(t, curr.Less(prev),
			"Current: %v is less than previous: %v", curr, prev)

		prev = curr
	}
}

func TestPairingHeapPushBulk(t *testing.T) {
	heap := pairing.New()

	heap.Push(IntKey(15))
	heap.Push(IntKey(20))
	heap.Push(IntKey(3))
	heap.Push(IntKey(1))

	actualValue := heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(1), actualValue, "actualValue should be 1")

	actualValue = heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(3), actualValue, "actualValue should be 3")

	actualValue = heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(15), actualValue, "actualValue should be 15")

	assert.Equal(t, 1, heap.Size(), "Size should be 1")
}

func TestPairingHeapRandom(t *testing.T) {
	heap := pairing.New()

	for i := 0; i < 1000; i++ {
		r, _ := rand.Int(rand.Reader, big.NewInt(30))
		heap.Push(IntKey(r.Int64()))
	}

	prev := heap.Pop()
	assert.NotNil(t, prev, "prev should not be nil")

	for heap.Size() != 0 {
		curr := heap.Pop()
		assert.NotNil(t, curr, "curr should not be nil")

		assert.True(t, prev.Less(curr) || prev.Eq(curr),
			"Heap property invalidated. prev: %v current: %v", prev, curr)

		prev = curr
	}
}

func TestString(t *testing.T) {
	heap := pairing.New()

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	actual := heap.String()
	expected := "PairingHeap:\n(1, 21), (1, 14), (1, 7), (1, 18), (0, 3)"

	assert.Equal(t, expected, actual)
}

func TestValues(t *testing.T) {
	heap := pairing.New()

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	actual := fmt.Sprintf("%v", heap.Values())
	expected := "[21 14 7 18 3]"

	assert.Equal(t, expected, actual)
}

func TestSize(t *testing.T) {
	heap := pairing.New()
	assert.Equal(t, 0, heap.Size())

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	assert.Equal(t, 5, heap.Size())
}

func TestPeek(t *testing.T) {
	heap := pairing.New()

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	actual := heap.Peek()
	assert.Equal(t, IntKey(3), actual)
}

func TestClear(t *testing.T) {
	heap := pairing.New()

	heap.Clear()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))
	assert.Equal(t, "[21 14 7 18 3]", fmt.Sprintf("%v", heap.Values()))

	heap.Clear()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))
	assert.Equal(t, "[21 14 7 18 3]", fmt.Sprintf("%v", heap.Values()))
}
