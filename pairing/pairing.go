package pairing

import (
	"fmt"
	"strings"
)

// Pairing implements the Heap interface.
type Pairing struct {
	root          *Node
	elementsCount int
}

// New returns an empty min pair heap.
func New() Heap {
	return &Pairing{root: nil, elementsCount: 0}
}

// Push adds values to our heap.
func (h *Pairing) Push(values ...Key) {
	for i := 0; i < len(values); i++ {
		if values[i] != nil {
			h.Add(values[i])
		}
	}
}

// Add a value into our heap.
func (h *Pairing) Add(val Key) {
	if val == nil {
		return
	}

	newNode := Node{Value: val, Children: make([]Node, 0)}
	h.elementsCount++

	if h.root == nil {
		h.root = &newNode
	} else {
		h.root = h.root.Merge(&newNode)
	}
}

// Pop removes the min element of this min heap.
func (h *Pairing) Pop() Key {
	if h == nil || h.root == nil {
		return nil
	}

	result := h.root.Value

	h.elementsCount--
	h.root = mergePairs(h.root.Children)

	return result
}

// mergePairs will combine all nodes in its input slice into a single node
// it  achieves this by merging them pair by pair.
func mergePairs(input []Node) *Node {
	if len(input) == 0 {
		return nil
	}

	if len(input) == 1 {
		return &input[0]
	}

	first := input[0].Merge(&input[1])
	second := mergePairs(input[2:])

	return first.Merge(second)
}

// Clear removes all elements from this heap.
func (h *Pairing) Clear() {
	h.root = nil
	h.elementsCount = 0
}

// String returns a string representation of this heap.
func (h *Pairing) String() string {
	values := make([]string, 0, h.elementsCount)

	Traverse(h.root, func(n *Node, level int) {
		values = append(values, fmt.Sprintf("(%d, %s)", level, n.Value))
	}, 0)

	return "PairingHeap:\n" + strings.Join(values, ", ")
}

// Values returns all elements in the heap.
func (h *Pairing) Values() []Key {
	result := make([]Key, 0, h.elementsCount)

	Traverse(h.root, func(n *Node, level int) {
		result = append(result, n.Value)
	}, 0)

	return result
}

// Size returns the number of elements in this heap.
func (h *Pairing) Size() int {
	if h == nil {
		return 0
	}

	return h.elementsCount
}

// Peek returns the smallest element without removing it, (nil, false) if heap is empty.
func (h *Pairing) Peek() Key {
	if h.Size() == 0 {
		return nil
	}

	return h.root.Value
}
