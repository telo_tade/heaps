package minmax_test

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/heaps/minmax"
)

// IntKey implement a key.
type IntKey int

// Less method for IntKey.
func (i IntKey) Less(k minmax.Key) bool {
	return i < k.(IntKey)
}

// Eq method for IntKey.
func (i IntKey) Eq(k minmax.Key) bool {
	return i == k.(IntKey)
}

func (i IntKey) String() string {
	return strconv.Itoa(int(i))
}

func TestMinMaxHeapPushConsecutive(t *testing.T) {
	heap := minmax.New()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	for i := 0; i < 10; i++ {
		heap.Push(IntKey(i))
	}

	for i := 0; i < 10; i++ {
		current := heap.Pop()

		assert.NotNil(t, current, "current should not be nil")
		assert.False(t, IntKey(9-i).Eq(current), "Got %v expected %v", current, 9-i)
	}
}

func TestMinMaxHeapPush(t *testing.T) {
	heap := minmax.New()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(0))
	heap.Push(IntKey(1))
	heap.Push(IntKey(2))
	heap.Push(IntKey(3))
	heap.Push(IntKey(4))
	heap.Push(IntKey(5))
	heap.Push(IntKey(6))
	heap.Push(IntKey(7))
	heap.Push(IntKey(8))
	heap.Push(IntKey(1))

	prev := heap.Pop()
	assert.NotNil(t, prev, "prev should not be nil")

	for heap.Size() != 0 {
		curr := heap.Pop()

		assert.NotNil(t, curr, "curr should not be nil")
		assert.False(t, curr.Less(prev),
			"Current: %v is less than previous: %v", curr, prev)

		prev = curr
	}
}

func TestMinMaxHeapPushBulk(t *testing.T) {
	heap := minmax.New()

	heap.Push(IntKey(15))
	heap.Push(IntKey(20))
	heap.Push(IntKey(3))
	heap.Push(IntKey(1))

	actualValue := heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(1), actualValue, "actualValue should be 1")

	actualValue = heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(3), actualValue, "actualValue should be 3")

	actualValue = heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(15), actualValue, "actualValue should be 15")

	actualValue = heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(20), actualValue, "actualValue should be 20")
}

func TestMinMaxHeapPop(t *testing.T) {
	heap := minmax.New()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3))
	heap.Push(IntKey(2))
	heap.Push(IntKey(1))

	actualValue := heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(1), actualValue, "actualValue should be 1")

	actualValue = heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(2), actualValue, "actualValue should be 2")

	actualValue = heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(3), actualValue, "actualValue should be 3")

	actualValue = heap.Pop()
	assert.Nil(t, actualValue, "actualValue should be nil")
	assert.Nil(t, actualValue, "actualValue should be nil")
	assert.Equal(t, 0, heap.Size(), "heap should be empty")
}

func TestMinMaxHeapRandom(t *testing.T) {
	heap := minmax.New()

	for i := 0; i < 1000; i++ {
		r, _ := rand.Int(rand.Reader, big.NewInt(30))
		heap.Push(IntKey(r.Int64()))
	}

	prev := heap.Pop()
	assert.NotNil(t, prev, "prev should not be nil")

	for heap.Size() != 0 {
		curr := heap.Pop()
		assert.NotNil(t, curr, "curr should not be nil")

		assert.True(t, prev.Less(curr) || prev.Eq(curr),
			"Heap property invalidated. prev: %v current: %v", prev, curr)

		prev = curr
	}
}

func TestMinMaxHeapRandomPopMax(t *testing.T) {
	heap := minmax.New()

	for i := 0; i < 1000; i++ {
		r, _ := rand.Int(rand.Reader, big.NewInt(30))
		heap.Push(IntKey(r.Int64()))
	}

	peeked := heap.PeekMax()
	assert.NotNil(t, peeked, "peeked should not be nil")

	prev := heap.PopMax()
	assert.NotNil(t, prev, "prev should not be nil")

	assert.True(t, prev.Eq(peeked),
		"PeekMax %v does not equal PopMax %v", peeked, prev)

	for heap.Size() != 0 {
		peeked := heap.PeekMax()
		curr := heap.PopMax()

		assert.True(t, curr.Eq(peeked),
			"PeekMax %v does not equal PopMax %v", peeked, prev)

		assert.False(t, prev.Less(curr),
			"Heap property invalidated. prev: %v current: %v", prev, curr)

		prev = curr
	}
}

func TestParent(t *testing.T) {
	heap := minmax.New()

	assert.Equal(t, 0, heap.Parent(-5), "Wrong parent value")
	assert.Equal(t, 8, heap.Parent(18), "Wrong parent value")
	assert.Equal(t, 9, heap.Parent(19), "Wrong parent value")
}

func TestLeft(t *testing.T) {
	heap := minmax.New()

	assert.Equal(t, 0, heap.Left(-5), "Wrong left value")
	assert.Equal(t, 11, heap.Left(5), "Wrong left value")
	assert.Equal(t, 13, heap.Left(6), "Wrong left value")
}

func TestRight(t *testing.T) {
	heap := minmax.New()

	assert.Equal(t, 0, heap.Right(-5), "Wrong right value")
	assert.Equal(t, 18, heap.Right(8), "Wrong right value")
	assert.Equal(t, 20, heap.Right(9), "Wrong right value")
}

func TestString(t *testing.T) {
	heap := minmax.New()

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	actual := heap.String()
	expected := "MinMaxHeap:\n3, 21, 14, 7, 18"

	assert.Equal(t, expected, actual)
}

func TestValues(t *testing.T) {
	heap := minmax.New()

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	actual := fmt.Sprintf("%v", heap.Values())
	expected := "[3 21 14 7 18]"

	assert.Equal(t, expected, actual)
}

func TestSize(t *testing.T) {
	heap := minmax.New()
	assert.Equal(t, 0, heap.Size())

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	assert.Equal(t, 5, heap.Size())
}

func TestPeek(t *testing.T) {
	heap := minmax.New()

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	actual := heap.Peek()
	assert.Equal(t, IntKey(3), actual)
}

func TestClear(t *testing.T) {
	heap := minmax.New()
	heap.Clear()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))
	assert.Equal(t, "[3 21 14 7 18]", fmt.Sprintf("%v", heap.Values()))

	heap.Clear()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))
	assert.Equal(t, "[3 21 14 7 18]", fmt.Sprintf("%v", heap.Values()))
}
