package minmax

import (
	"fmt"
	"math"
	"strings"
)

// MinMax implements the Heap interface.
type MinMax struct {
	list []Key
}

// New returns a new heap with 0 elements.
func New() Heap {
	return &MinMax{list: make([]Key, 0)}
}

// Parent returns the parent node's index.
func (h *MinMax) Parent(index int) int {
	if index <= 0 {
		return 0
	}

	return (index - 1) / 2
}

// Left returns the left child's index.
func (h *MinMax) Left(index int) int {
	if index < 0 {
		return 0
	}

	return 2*index + 1
}

// Right returns the right child's index.
func (h *MinMax) Right(index int) int {
	if index < 0 {
		return 0
	}

	return 2*index + 2
}

// Heapify takes a slices and returns a heap based on that slice.
func (h *MinMax) Heapify(input []Key) Heap {
	result := &MinMax{list: input}

	for i := len(result.list) / 2; i >= 0; i-- {
		h.bubbleDown(result.list, i)
	}

	return result
}

func (h *MinMax) bubbleDown(input []Key, index int) {
	if index >= len(input) {
		return
	}

	if h.isMinLevel(index) {
		h.bubbleDownMin(input, index)
	} else {
		h.bubbleDownMax(input, index)
	}
}

func (h *MinMax) isMinLevel(index int) bool {
	level := math.Log2(float64(index + 1))

	return int(level)%2 == 0
}

func (h *MinMax) bubbleDownMin(input []Key, index int) {
	if 2*index+1 >= len(input) {
		return
	}

	smallest := h.findSmallestChildOrGrandchild(input, index)

	if smallest > 2 && index == h.Parent(h.Parent(smallest)) {
		if input[smallest].Less(input[index]) {
			input[smallest], input[index] = input[index], input[smallest]

			if input[h.Parent(smallest)].Less(input[smallest]) {
				input[h.Parent(smallest)], input[smallest] = input[smallest], input[h.Parent(smallest)]
			}

			h.bubbleDownMin(input, smallest)
		}
	} else {
		if input[smallest].Less(input[index]) {
			input[smallest], input[index] = input[index], input[smallest]
		}
	}
}

func (h *MinMax) findLargestChildOrGrandchild(input []Key, index int) int {
	children := []int{
		h.Left(index), h.Right(index),
		h.Left(h.Left(index)), h.Right(h.Left(index)),
		h.Left(h.Right(index)), h.Right(h.Right(index)),
	}
	largest := children[0]

	for i := 1; i < len(children) && children[i] < len(input); i++ {
		if input[largest].Less(input[children[i]]) {
			largest = children[i]
		}
	}

	return largest
}

func (h *MinMax) findSmallestChildOrGrandchild(input []Key, index int) int {
	children := []int{
		h.Left(index), h.Right(index),
		h.Left(h.Left(index)), h.Right(h.Left(index)),
		h.Left(h.Right(index)), h.Right(h.Right(index)),
	}
	smallest := children[0]

	for i := 1; i < len(children) && children[i] < len(input); i++ {
		if input[children[i]].Less(input[smallest]) {
			smallest = children[i]
		}
	}

	return smallest
}

func (h *MinMax) bubbleDownMax(input []Key, index int) {
	if h.Left(index) >= len(input) {
		return
	}

	largest := h.findLargestChildOrGrandchild(input, index)

	if largest > 2 && index == h.Parent(h.Parent(largest)) {
		if input[index].Less(input[largest]) {
			input[largest], input[index] = input[index], input[largest]

			if input[largest].Less(input[h.Parent(largest)]) {
				input[h.Parent(largest)], input[largest] = input[largest], input[h.Parent(largest)]
			}

			h.bubbleDownMax(input, largest)
		}
	} else {
		if input[index].Less(input[largest]) {
			input[largest], input[index] = input[index], input[largest]
		}
	}
}

// Add a value into our heap.
func (h *MinMax) Add(val Key) {
	if val == nil {
		return
	}

	h.list = append(h.list, val)

	h.bubbleUp(h.list, len(h.list)-1)
}

// Push adds a new element into the heap.
func (h *MinMax) Push(values ...Key) {
	if len(values) == 1 {
		h.Add(values[0])

		return
	}

	filtered := h.filterNils(values)
	h.list = append(h.list, filtered...)

	for i := len(h.list) - 1; i >= 0; i-- {
		h.bubbleDown(h.list, i)
	}
}

func (h *MinMax) filterNils(input []Key) []Key {
	result := make([]Key, 0, len(input))

	for i := 0; i < len(input); i++ {
		if input[i] != nil {
			result = append(result, input[i])
		}
	}

	return result
}

func (h *MinMax) bubbleUp(input []Key, index int) {
	if index == 0 {
		return
	}

	if h.isMinLevel(index) {
		if input[h.Parent(index)].Less(input[index]) {
			input[h.Parent(index)], input[index] = input[index], input[h.Parent(index)]

			h.bubbleUpMax(input, h.Parent(index))
		} else {
			h.bubbleUpMin(input, index)
		}
	} else {
		if input[index].Less(input[h.Parent(index)]) {
			input[h.Parent(index)], input[index] = input[index], input[h.Parent(index)]

			h.bubbleUpMin(input, h.Parent(index))
		} else {
			h.bubbleUpMax(input, index)
		}
	}
}

func (h *MinMax) bubbleUpMin(input []Key, index int) {
	if index < 3 {
		return
	}

	grandparent := h.Parent(h.Parent(index))

	if input[index].Less(input[grandparent]) {
		input[index], input[grandparent] = input[grandparent], input[index]

		h.bubbleUpMin(input, grandparent)
	}
}

func (h *MinMax) bubbleUpMax(input []Key, index int) {
	if index < 3 {
		return
	}

	grandparent := h.Parent(h.Parent(index))

	if input[grandparent].Less(input[index]) {
		input[index], input[grandparent] = input[grandparent], input[index]

		h.bubbleUpMax(input, grandparent)
	}
}

// Pop removes the min from this minMaxHeap while maintaining its minMax and heap properties.
func (h *MinMax) Pop() Key {
	if h.list == nil || len(h.list) == 0 {
		return nil
	}

	result := h.list[0]

	h.list[0] = h.list[len(h.list)-1]
	h.list = h.list[0 : len(h.list)-1]

	h.bubbleDown(h.list, 0)

	return result
}

// Values returns the slices of elements contained in this minMax heap.
func (h *MinMax) Values() []Key {
	return h.list
}

// PopMax returns the max element of this MinMax heap.
func (h *MinMax) PopMax() Key {
	switch {
	case h.Size() == 0:
		return nil
	case len(h.list) == 1:
		result := h.list[0]
		h.list = make([]Key, 0)

		return result
	}

	resultIndex := 1
	if 1+resultIndex < len(h.list) && h.list[resultIndex].Less(h.list[1+resultIndex]) {
		resultIndex++
	}

	result := h.list[resultIndex]

	h.list[resultIndex] = h.list[len(h.list)-1]
	h.list = h.list[:len(h.list)-1]

	h.bubbleDown(h.list, resultIndex)

	return result
}

// PeekMax returns without removing the max element in this heap.
func (h *MinMax) PeekMax() Key {
	if h.Size() == 0 {
		return nil
	}

	if len(h.list) == 1 {
		return h.list[0]
	}

	resultIndex := 1
	if 1+resultIndex < len(h.list) && h.list[resultIndex].Less(h.list[1+resultIndex]) {
		resultIndex++
	}

	return h.list[resultIndex]
}

// Clear removes all elements from this heap.
func (h *MinMax) Clear() {
	h.list = []Key{}
}

// String returns a string representation of this heap.
func (h *MinMax) String() string {
	values := make([]string, 0, h.Size())

	for _, value := range h.list {
		values = append(values, fmt.Sprintf("%s", value))
	}

	return "MinMaxHeap:\n" + strings.Join(values, ", ")
}

// Size returns the number of elements in this heap.
func (h *MinMax) Size() int {
	return len(h.list)
}

// Peek returns the smallest element without removing it, (nil, false) if heap is empty.
func (h *MinMax) Peek() Key {
	if h.Size() == 0 {
		return nil
	}

	return h.list[0]
}
