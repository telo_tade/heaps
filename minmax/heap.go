package minmax

// Heap interface models the operations offered by a heap data structure.
type Heap interface {
	Add(val Key)
	Parent(index int) int
	Left(index int) int
	Right(index int) int
	Push(values ...Key)
	Pop() Key
	Clear()
	String() string
	Values() []Key
	Size() int
	Peek() Key
	PopMax() Key
	PeekMax() Key
}
