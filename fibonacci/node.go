package fibonacci

// Node models a node in our Fibonacci heap.
type Node struct {
	Value                             Key
	prev, next, firstChild, lastChild *Node
	degree                            int
}

// MoveValues takes all values of other and moves them to n.
func (n *Node) MoveValues(other *Node) {
	if other.firstChild == nil {
		return
	}

	n.degree += other.degree

	if n.firstChild == nil {
		n.firstChild, n.lastChild = other.firstChild, other.lastChild
	} else {
		other.firstChild.prev, n.lastChild.next = n.lastChild, other.firstChild
		n.lastChild = other.lastChild
	}

	other.firstChild, other.lastChild, other.degree = nil, nil, 0
}

// removeChild removes a child from n.
func (n *Node) removeChild(child *Node) {
	n.degree--

	if child.prev == nil {
		n.firstChild = child.next
	} else {
		child.prev.next = child.next
	}

	if child.next == nil {
		n.lastChild = child.prev
	} else {
		child.next.prev = child.prev
	}

	child.prev, child.next = nil, nil
}

// AppendLast adds child after n's last child.
func (n *Node) AppendLast(child *Node) {
	n.degree++

	if n.lastChild == nil {
		n.firstChild, n.lastChild = child, child

		return
	}

	child.prev, n.lastChild.next, n.lastChild = n.lastChild, child, child
}

// AppendFirst adds child before n's first child.
func (n *Node) AppendFirst(child *Node) {
	n.degree++

	if n.firstChild == nil {
		n.firstChild, n.lastChild = child, child

		return
	}

	child.next, n.firstChild.prev, n.firstChild = n.firstChild, child, child
}

// merge combines two of n's children.
func (n *Node) merge(first, second *Node) *Node {
	if second.Value.Less(first.Value) {
		first, second = second, first
	}

	n.removeChild(second)
	first.AppendLast(second)

	return first
}

// GetMin returns the smallest child.
func (n *Node) GetMin() *Node {
	if n.firstChild == nil {
		return nil
	}

	result := n.firstChild

	for c := n.firstChild.next; c != nil; c = c.next {
		if c.Value.Less(result.Value) {
			result = c
		}
	}

	return result
}

// CombineSameSize merges all children with the same degree.
func (n *Node) CombineSameSize() {
	sizes := make(map[int]*Node)
	nextItem := n.firstChild

	for nextItem != nil {
		current := nextItem
		nextItem = nextItem.next

		for sameSize, ok := sizes[current.degree]; ok; sameSize, ok = sizes[current.degree] {
			delete(sizes, current.degree)

			current = n.merge(current, sameSize)
		}

		sizes[current.degree] = current
	}
}

// Traverse first the children then the node then its siblings.
func Traverse(n *Node, f func(*Node, int), level int) {
	if n == nil {
		return
	}

	Traverse(n.firstChild, f, 1+level)

	f(n, level)

	Traverse(n.next, f, 1+level)
}
