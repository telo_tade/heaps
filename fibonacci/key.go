package fibonacci

// Key interface models a value stored in our pairing heap.
type Key interface {
	Less(Key) bool
	Eq(Key) bool
}
