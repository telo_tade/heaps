package fibonacci

import (
	"fmt"
	"strings"
)

// Fibonacci implements the Heap interface.
type Fibonacci struct {
	forest        Node
	elementsCount int
}

// New returns a new, empty heap.
func New() Heap {
	return &Fibonacci{}
}

// computeFirstChild finds the minimum root value subheap and makes it first child.
func (h *Fibonacci) computeFirstChild() {
	min := h.forest.GetMin()

	if min != nil {
		h.forest.removeChild(min)
		h.forest.AppendFirst(min)
	}
}

// Pop removes and returns the smallest value of this heap.
func (h *Fibonacci) Pop() Key {
	if h.forest.firstChild == nil {
		return nil
	}

	h.elementsCount--

	min := h.forest.firstChild
	h.forest.removeChild(min)
	h.forest.MoveValues(min)

	h.forest.CombineSameSize()
	h.computeFirstChild()

	return min.Value
}

// Add adds a new value into the heap.
func (h *Fibonacci) Add(val Key) {
	if val == nil {
		return
	}

	h.elementsCount++

	s := &Node{Value: val}
	if h.forest.firstChild == nil || s.Value.Less(h.forest.firstChild.Value) {
		h.forest.AppendFirst(s)

		return
	}

	h.forest.AppendLast(s)
}

// Push adds a value to our heap.
func (h *Fibonacci) Push(values ...Key) {
	for i := 0; i < len(values); i++ {
		if values[i] != nil {
			h.Add(values[i])
		}
	}
}

// Clear removes all elements from this heap.
func (h *Fibonacci) Clear() {
	h.forest = Node{}
	h.elementsCount = 0
}

// String returns a string representation of this heap.
func (h *Fibonacci) String() string {
	values := make([]string, 0, h.elementsCount)

	Traverse(&h.forest, func(n *Node, level int) {
		if n.Value != nil {
			values = append(values, fmt.Sprintf("(%d, %s)", level, n.Value))
		}
	}, 0)

	return "FibonacciHeap:\n" + strings.Join(values, ", ")
}

// Values returns all elements in this heap.
func (h *Fibonacci) Values() []Key {
	result := make([]Key, 0, h.elementsCount)

	Traverse(&h.forest, func(n *Node, level int) {
		result = append(result, n.Value)
	}, 0)

	return result
}

// Size returns the number of elements in this heap.
func (h *Fibonacci) Size() int {
	return h.elementsCount
}

// Peek returns the smallest element without removing it, (nil, false) if heap is empty.
func (h *Fibonacci) Peek() Key {
	if h == nil || h.forest.firstChild == nil {
		return nil
	}

	return h.forest.firstChild.Value
}
