package skew

// Node models a node in the heap (implemented as a binary tree).
type Node struct {
	value               Key
	left, right, parent *Node
}

// Traverse performs inorder traversal (left, root, right).
func Traverse(n *Node, f func(*Node, int), level int) {
	if n == nil {
		return
	}

	Traverse(n.left, f, 1+level)

	f(n, level)

	Traverse(n.right, f, 1+level)
}
