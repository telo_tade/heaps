package skew

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"strings"
)

func merge(h1, h2 *Node) *Node {
	if h1 == nil {
		return h2
	}

	if h2 == nil {
		return h1
	}

	if h1.value.Less(h2.value) {
		h1, h2 = h2, h1
	}

	h1.left = merge(h1.left, h2)

	if randomBool() {
		h1.left, h1.right = h1.right, h1.left
	}

	return h1
}

// Skew implements the Heap interface.
type Skew struct {
	root          *Node
	elementsCount int
}

// New instantiates a new empty heap tree.
func New() Heap {
	return &Skew{root: nil, elementsCount: 0}
}

// Add adds a value onto the heap and bubbles it up accordingly.
func (h *Skew) Add(val Key) {
	if val == nil {
		return
	}

	h.elementsCount++

	newNode := Node{value: val, left: nil, right: nil, parent: nil}

	if h.root == nil {
		h.root = &newNode

		return
	}

	h.root = merge(h.root, &newNode)
}

func randomBool() bool {
	r, _ := rand.Int(rand.Reader, big.NewInt(100))

	return r.Int64()%2 == 0
}

// Push adds values onto the heap.
func (h *Skew) Push(values ...Key) {
	for i := range values {
		h.Add(values[i])
	}
}

// Pop removes top element on heap and returns it, or nil if heap is empty.
// Second return parameter is true, unless the heap was empty and there was nothing to pop.
func (h *Skew) Pop() Key {
	if h.elementsCount == 0 {
		return nil
	}

	result := h.root.value

	h.root = merge(h.root.left, h.root.right)
	h.elementsCount--

	return result
}

// Clear removes all elements from the heap.
func (h *Skew) Clear() {
	h.root = nil
	h.elementsCount = 0
}

// String returns a string representation of container.
func (h *Skew) String() string {
	values := make([]string, 0, h.elementsCount)

	Traverse(h.root, func(n *Node, level int) {
		values = append(values, fmt.Sprintf("(%d, %s)", level, n.value))
	}, 0)

	return "SkewHeap:\n" + strings.Join(values, ", ")
}

// Values returns all elements in the heap.
func (h *Skew) Values() []Key {
	result := make([]Key, 0, h.elementsCount)

	Traverse(h.root, func(n *Node, level int) {
		result = append(result, n.value)
	}, 0)

	return result
}

// Size returns number of elements within the heap.
func (h *Skew) Size() int {
	return h.elementsCount
}

// Peek returns the biggest element without removing it, (nil, false) if heap is empty.
func (h *Skew) Peek() Key {
	if h.elementsCount == 0 {
		return nil
	}

	return h.root.value
}
