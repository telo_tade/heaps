package treap

// Key models an element that can be inserted in the heap.
type Key interface {
	Less(Key) bool
	Eq(Key) bool
}
