package treap

// Treap interface models the operations offered by a treap data structure.
type Treap interface {
	Add(val Key)
	Push(values ...Key)
	Search(val Key) *Node
	Delete(value Key)
	Min() Key
	Max() Key
	Clear()
	String() string
	Values() []Key
	Size() int
	Peek() (Key, int, bool)
	Root() *Node
}
