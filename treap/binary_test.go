package treap_test

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"regexp"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/telo_tade/heaps/treap"
)

type IntKey int

func (i IntKey) Less(k treap.Key) bool {
	return i < k.(IntKey)
}

func (i IntKey) Eq(k treap.Key) bool {
	return i == k.(IntKey)
}

func (i IntKey) String() string {
	return strconv.Itoa(int(i))
}

func isValidBST(root *treap.Node) bool {
	if root == nil {
		return true
	}

	if root.Left != nil && root.Value.Less(root.Left.Value) {
		return false
	}

	if root.Right != nil && !root.Value.Less(root.Right.Value) && !root.Value.Eq(root.Right.Value) {
		return false
	}

	return isValidBST(root.Left) && isValidBST(root.Right)
}

func isValidMinHeap(root *treap.Node) bool {
	if root == nil {
		return true
	}

	if root.Left != nil && root.Priority > root.Left.Priority {
		return false
	}

	if root.Right != nil && root.Priority > root.Right.Priority {
		return false
	}

	return isValidMinHeap(root.Left) && isValidMinHeap(root.Right)
}

func TestTreapIsValidRandomTreap(t *testing.T) {
	treap := treap.New()

	for i := 0; i < 1000; i++ {
		r, _ := rand.Int(rand.Reader, big.NewInt(10000))
		treap.Push(IntKey(r.Int64()))
	}

	assert.True(t, isValidBST(treap.Root()), "Invalid BST")
	assert.True(t, isValidMinHeap(treap.Root()), "Invalid min heap")
}

func TestTreapContainsRandomValues(t *testing.T) {
	values := make([]IntKey, 1000)

	for i := 0; i < len(values); i++ {
		r, _ := rand.Int(rand.Reader, big.NewInt(10000))

		values[i] = IntKey(r.Int64())
	}

	treap := treap.New()

	for i := 0; i < len(values); i++ {
		treap.Push(values[i])
	}

	for i := 0; i < len(values); i++ {
		assert.NotNil(t, treap.Search(values[i]), "should not be nil")
	}

	assert.True(t, isValidBST(treap.Root()), "Invalid BST")
	assert.True(t, isValidMinHeap(treap.Root()), "Invalid min heap")
}

func TestTreapDeleteRandom(t *testing.T) {
	values := make([]IntKey, 1000)

	for i := 0; i < len(values); i++ {
		r, _ := rand.Int(rand.Reader, big.NewInt(10000))

		values[i] = IntKey(r.Int64())
	}

	values[len(values)/2] = IntKey(2020)

	treap := treap.New()

	for i := 0; i < len(values); i++ {
		treap.Push(values[i])
	}

	for i := 0; i < len(values); i++ {
		assert.NotNil(t, treap.Search(values[i]), "should not be nil")
	}

	assert.True(t, isValidBST(treap.Root()), "Invalid BST")
	assert.True(t, isValidMinHeap(treap.Root()), "Invalid min heap")

	treap.Delete(IntKey(2020))

	assert.True(t, isValidBST(treap.Root()), "Invalid BST")
	assert.True(t, isValidMinHeap(treap.Root()), "Invalid min heap")

	for treap.Search(IntKey(2020)) != nil {
		treap.Delete(IntKey(2020))

		assert.True(t, isValidBST(treap.Root()), "should be valid BST")
		assert.True(t, isValidMinHeap(treap.Root()), "should be valid min heap")
	}

	treap.Push(IntKey(2020))

	assert.NotNil(t, treap.Search(IntKey(2020)), "should not be nil")
	assert.True(t, isValidBST(treap.Root()), "Invalid BST")
	assert.True(t, isValidMinHeap(treap.Root()), "Invalid min heap")

	treap.Delete(IntKey(2020))

	assert.True(t, isValidBST(treap.Root()), "Invalid BST")
	assert.True(t, isValidMinHeap(treap.Root()), "Invalid min heap")
}

func TestTreapMin(t *testing.T) {
	treap := buildTreap(1000, 500)

	assert.True(t, isValidBST(treap.Root()), "Invalid BST")
	assert.True(t, isValidMinHeap(treap.Root()), "Invalid min heap")

	prevMin := treap.Min()
	treap.Delete(prevMin)

	assert.True(t, isValidBST(treap.Root()), "Invalid BST")
	assert.True(t, isValidMinHeap(treap.Root()), "Invalid min heap")

	for i := 1; i < 1000; i++ {
		min := treap.Min()
		assert.False(t, min.Less(prevMin), "should not be less")

		treap.Delete(min)

		assert.True(t, isValidBST(treap.Root()), "Invalid BST")
		assert.True(t, isValidMinHeap(treap.Root()), "Invalid min heap")

		prevMin = min
	}
}

func buildTreap(elementsCount int, maxElement int64) treap.Treap {
	result := treap.New()

	for i := 0; i < elementsCount; i++ {
		r, _ := rand.Int(rand.Reader, big.NewInt(maxElement))

		result.Push(IntKey(r.Int64()))
	}

	return result
}

func TestTreapMax(t *testing.T) {
	treap := buildTreap(1000, 500)

	assert.True(t, isValidBST(treap.Root()), "Invalid BST")
	assert.True(t, isValidMinHeap(treap.Root()), "Invalid min heap")

	prevMax := treap.Max()
	treap.Delete(prevMax)

	assert.True(t, isValidBST(treap.Root()), "Invalid BST")
	assert.True(t, isValidMinHeap(treap.Root()), "Invalid min heap")

	for i := 1; i < 1000; i++ {
		max := treap.Max()
		assert.False(t, prevMax.Less(max), "should not be min")

		treap.Delete(max)

		assert.True(t, isValidBST(treap.Root()), "Invalid BST")
		assert.True(t, isValidMinHeap(treap.Root()), "Invalid min heap")

		prevMax = max
	}
}

func TestTreapDeleteDuplicates(t *testing.T) {
	treap := treap.New()
	values := make([]IntKey, 1000)

	for i := 0; i < len(values); i++ {
		r, _ := rand.Int(rand.Reader, big.NewInt(100))

		values[i] = IntKey(r.Int64())

		treap.Push(values[i])

		assert.True(t, isValidBST(treap.Root()), "Invalid BST")
		assert.True(t, isValidMinHeap(treap.Root()), "Invalid min heap")
	}

	for i := 0; i < len(values); i++ {
		treap.Delete(values[i])

		assert.True(t, isValidBST(treap.Root()), "Invalid BST")
		assert.True(t, isValidMinHeap(treap.Root()), "Invalid min heap")
	}
}

func TestString(t *testing.T) {
	heap := treap.New()

	heap.Push(IntKey(3))
	heap.Push(IntKey(4))
	heap.Push(IntKey(5))

	actual := heap.String()
	expected := "BinaryHeap:\\s\\(\\d, 3\\), \\(\\d, 4\\), \\(\\d, 5\\)"
	matched, err := regexp.MatchString(expected, actual)

	assert.True(t, matched, "results should match")
	assert.Nil(t, err, "error should be nil")
}

func TestValues(t *testing.T) {
	heap := treap.New()

	heap.Push(IntKey(3))
	heap.Push(IntKey(4))
	heap.Push(IntKey(5))

	actual := fmt.Sprintf("%v", heap.Values())
	expected := "[3 4 5]"

	assert.Equal(t, expected, actual)
}

func TestSize(t *testing.T) {
	heap := treap.New()
	assert.Equal(t, 0, heap.Size())

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	assert.Equal(t, 5, heap.Size())
}

func TestPeek(t *testing.T) {
	heap := treap.New()

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	key, firstPriority, found := heap.Peek()
	assert.Equal(t, true, found)
	assert.NotEqual(t, nil, key)

	heap.Delete(key)

	_, secondPriority, found := heap.Peek()
	assert.Equal(t, true, found)

	assert.Equal(t, true, firstPriority < secondPriority)
}

func TestClear(t *testing.T) {
	heap := treap.New()

	heap.Clear()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))
	assert.Equal(t, 5, heap.Size())

	heap.Clear()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))
	assert.Equal(t, 5, heap.Size())
}
