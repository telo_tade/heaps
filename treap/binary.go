package treap

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"strings"
)

// Binary implements the Treap interface.
type Binary struct {
	root          *Node
	elementsCount int
}

// New returns a new empty treap.
func New() Treap {
	return &Binary{root: nil, elementsCount: 0}
}

// Add adds a value to this treap.
func (t *Binary) Add(val Key) {
	if val == nil {
		return
	}

	t.elementsCount++

	r, _ := rand.Int(rand.Reader, big.NewInt(100))
	newNode := Node{Value: val, Priority: int(r.Int64())}

	if t.root == nil {
		t.root = &newNode
	} else {
		t.root = t.root.Add(&newNode)
	}
}

// Push adds values onto the heap.
func (t *Binary) Push(values ...Key) {
	for i := range values {
		t.Add(values[i])
	}
}

// Search returns the node containing a given value, or nil.
func (t *Binary) Search(val Key) *Node {
	if t == nil || t.root == nil {
		return nil
	}

	return t.root.Search(val)
}

// Delete the given value from this treap.
func (t *Binary) Delete(value Key) {
	if t.Search(value) == nil {
		return
	}

	t.elementsCount--
	t.root = t.root.Delete(value)
}

// Min finds the minimum value in our treap.
func (t *Binary) Min() Key {
	return t.root.Min()
}

// Max finds the minimum value in our treap.
func (t *Binary) Max() Key {
	return t.root.Max()
}

// Clear removes all elements from the heap.
func (t *Binary) Clear() {
	t.root = nil
	t.elementsCount = 0
}

// String returns a string representation of this heap.
func (t *Binary) String() string {
	values := make([]string, 0, t.elementsCount)

	Traverse(t.root, func(n *Node, level int) {
		values = append(values, fmt.Sprintf("(%d, %s)", level, n.Value))
	}, 0)

	return "BinaryHeap:\n" + strings.Join(values, ", ")
}

// Values returns all elements in the heap.
func (t *Binary) Values() []Key {
	result := make([]Key, 0, t.elementsCount)

	Traverse(t.root, func(n *Node, level int) {
		result = append(result, n.Value)
	}, 0)

	return result
}

// Size returns the elements count of this treap.
func (t *Binary) Size() int {
	return t.elementsCount
}

// Peek returns the smalles priority element without removing it, (nil, false) if heap is empty.
func (t *Binary) Peek() (Key, int, bool) {
	if t.elementsCount == 0 {
		return nil, 0, false
	}

	return t.root.Value, t.root.Priority, true
}

// Root returns the root of this treap.
func (t *Binary) Root() *Node {
	return t.root
}
