package treap

// Node models a node in this treap.
type Node struct {
	Value       Key
	Priority    int
	Left, Right *Node
}

// Add adds a new node to this node's subtree.
func (n *Node) Add(newNode *Node) *Node {
	if n == nil {
		return newNode
	}

	if n.Value.Less(newNode.Value) {
		n.Right = n.Right.Add(newNode)

		if n.Right.Priority < n.Priority {
			n = n.rotateLeft()
		}
	} else {
		n.Left = n.Left.Add(newNode)

		if n.Left.Priority < n.Priority {
			n = n.rotateRight()
		}
	}

	return n
}

func (n *Node) rotateRight() *Node {
	result, leftRight := n.Left, n.Left.Right

	result.Right = n
	n.Left = leftRight

	return result
}

func (n *Node) rotateLeft() *Node {
	result, rightLeft := n.Right, n.Right.Left

	result.Left = n
	n.Right = rightLeft

	return result
}

// Search looks for a given value in this node's subtree.
func (n *Node) Search(val Key) *Node {
	if n == nil || n.Value.Eq(val) {
		return n
	}

	if n.Value.Less(val) {
		return n.Right.Search(val)
	}

	return n.Left.Search(val)
}

// Min returns the minimum value in this node's subtree.
func (n *Node) Min() Key {
	if n == nil {
		return nil
	}

	if n.Left == nil {
		return n.Value
	}

	return n.Left.Min()
}

// Max returns the minimum value in this node's subtree.
func (n *Node) Max() Key {
	if n == nil {
		return nil
	}

	if n.Right == nil {
		return n.Value
	}

	return n.Right.Max()
}

func (n *Node) join(other *Node) *Node {
	var result *Node

	joinRecursive(&result, n, other)

	return result
}

func joinRecursive(result **Node, small, big *Node) {
	if small == nil {
		*result = big

		return
	}

	if big == nil {
		*result = small

		return
	}

	if small.Priority <= big.Priority {
		*result = &Node{small.Value, small.Priority, small.Left, nil}

		joinRecursive(&(*result).Right, small.Right, big)
	} else {
		*result = &Node{big.Value, big.Priority, nil, big.Right}

		joinRecursive(&(*result).Left, small, big.Left)
	}
}

// Delete removes a value from this node's subtree.
func (n *Node) Delete(value Key) *Node {
	small, big := n.Split(value)

	return small.join(big)
}

// Split returns a treap with all elements smaller than value, and one with larger.
func (n *Node) Split(value Key) (*Node, *Node) {
	var small, big *Node

	n.splitRecursive(value, &small, &big)

	return small, big
}

func (n *Node) splitRecursive(value Key, small, big **Node) {
	if n == nil {
		*small, *big = nil, nil

		return
	}

	switch {
	case n.Value.Less(value):
		*small = &Node{n.Value, n.Priority, n.Left, nil}

		n.Right.splitRecursive(value, &(*small).Right, big)
	case value.Less(n.Value):
		*big = &Node{n.Value, n.Priority, nil, n.Right}

		n.Left.splitRecursive(value, small, &(*big).Left)
	default:
		*small, *big = n.Left, n.Right
	}
}

// Traverse performs inorder traversal (left, root, right).
func Traverse(n *Node, f func(*Node, int), level int) {
	if n == nil {
		return
	}

	Traverse(n.Left, f, 1+level)

	f(n, level)

	Traverse(n.Right, f, 1+level)
}
