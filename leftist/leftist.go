package leftist

import (
	"fmt"
	"strings"
)

// Leftist implements the Heap interface.
type Leftist struct {
	root          *Node
	elementsCount int
}

// New returns an empty heap.
func New() Heap {
	return &Leftist{}
}

// Add a value into our heap.
func (h *Leftist) Add(val Key) {
	if val == nil {
		return
	}

	newNode := Node{Value: val, distanceToLeaf: 1}

	h.root = h.root.Merge(&newNode)
	h.elementsCount++
}

// Push adds values to our heap.
func (h *Leftist) Push(values ...Key) {
	for i := 0; i < len(values); i++ {
		if values[i] != nil {
			h.Add(values[i])
		}
	}
}

// Pop removes and returns the minimum value of this heap.
func (h *Leftist) Pop() Key {
	if h == nil || h.root == nil {
		return nil
	}

	result := h.root.Value

	h.root = h.root.left.Merge(h.root.right)
	h.elementsCount--

	return result
}

// Clear removes all elements from this heap.
func (h *Leftist) Clear() {
	h.root = nil
	h.elementsCount = 0
}

// String returns a string representation of this heap.
func (h *Leftist) String() string {
	values := make([]string, 0, h.elementsCount)

	Traverse(h.root, func(n *Node, level int) {
		values = append(values, fmt.Sprintf("(%d, %s)", level, n.Value))
	}, 0)

	return "LeftistHeap:\n" + strings.Join(values, ", ")
}

// Values returns all elements in the heap.
func (h *Leftist) Values() []Key {
	result := make([]Key, 0, h.elementsCount)

	Traverse(h.root, func(n *Node, level int) {
		result = append(result, n.Value)
	}, 0)

	return result
}

// Size returns the number of elements in this heap.
func (h *Leftist) Size() int {
	return h.elementsCount
}

// Peek returns the smallest element without removing it, (nil, false) if heap is empty.
func (h *Leftist) Peek() Key {
	if h.root == nil {
		return nil
	}

	return h.root.Value
}

// Root returns the root node.
func (h *Leftist) Root() *Node {
	return h.root
}
