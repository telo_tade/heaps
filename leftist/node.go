package leftist

// Node models a node in our Fibonacci heap.
type Node struct {
	Value          Key
	left, right    *Node
	distanceToLeaf int
}

// Merge combines the elements in n's subtree with that of other.
func (n *Node) Merge(other *Node) *Node {
	if n == nil {
		return other
	}

	if other == nil {
		return n
	}

	if other.Value.Less(n.Value) {
		n, other = other, n
	}

	n.right = n.right.Merge(other)

	if n.left == nil {
		n.left = other
		n.right = nil

		return n
	}

	if n.left.distanceToLeaf < n.right.distanceToLeaf {
		n.left, n.right = n.right, n.left
	}

	n.distanceToLeaf = 1 + n.right.distanceToLeaf

	return n
}

// DistanceToLeaf returns the distance to leaf of this node.
func (n *Node) DistanceToLeaf() int {
	return n.distanceToLeaf
}

// Left returns the left child of this node.
func (n *Node) Left() *Node {
	return n.left
}

// Right returns the right of this node.
func (n *Node) Right() *Node {
	return n.right
}

// Traverse the heap rooted in n.
func Traverse(n *Node, f func(*Node, int), level int) {
	if n == nil {
		return
	}

	Traverse(n.left, f, 1+level)

	f(n, level)

	Traverse(n.right, f, 1+level)
}
