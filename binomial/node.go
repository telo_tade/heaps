package binomial

// Node mmodels a node in the binomial heap.
type Node struct {
	value Key

	parent, leftmostChild, sibling *Node

	elementsCount int
}

func (n *Node) addChild(other *Node) {
	other.parent = n

	if n.leftmostChild == nil {
		n.leftmostChild = other

		return
	}

	n.leftmostChild = addToList(n.leftmostChild, other)
}

// merges 2 nodes into a single one, maintaining the max heap property.
func merge(first, second *Node) *Node {
	if second.value.Less(first.value) {
		first.elementsCount += second.elementsCount
		first.addChild(second)

		return first
	}

	second.elementsCount += first.elementsCount
	second.addChild(first)

	return second
}

func addToList(listHead, node *Node) *Node {
	if listHead == nil {
		return node
	}

	var current, prev *Node
	prev = nil
	current = listHead

	for current != nil && node.value.Less(current.value) {
		prev = current
		current = current.sibling
	}

	if prev == nil {
		node.sibling = listHead

		return node
	}

	prev.sibling, node.sibling = node, current

	return listHead
}

// findElementsCount iterates through a linked list looking for a node with the given elements count.
func findElementsCount(listHead *Node, count int) *Node {
	if listHead == nil || listHead.elementsCount == count {
		return listHead
	}

	for listHead != nil && listHead.elementsCount != count {
		listHead = listHead.sibling
	}

	return listHead
}

func removeFromList(listHead, node *Node) *Node {
	if listHead == nil {
		return nil
	}

	if listHead == node {
		return listHead.sibling
	}

	current := listHead
	for current.sibling != nil && current.sibling != node {
		current = current.sibling
	}

	if current.sibling == nil {
		return listHead
	}

	current.sibling = node.sibling

	return listHead
}

// Traverse first the children then the siblings.
func Traverse(n *Node, f func(*Node, int), level int) {
	if n == nil {
		return
	}

	Traverse(n.leftmostChild, f, 1+level)

	f(n, level)

	Traverse(n.sibling, f, 1+level)
}
