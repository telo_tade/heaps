package binomial

// Key models an element of the min max heap.
type Key interface {
	Less(Key) bool
	Eq(Key) bool
}
