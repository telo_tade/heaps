package binomial

import (
	"fmt"
	"strings"
)

// Binomial implements the Heap interface.
type Binomial struct {
	listHead *Node

	elementsCount int
}

// New returns a binomial heap with 0 elements.
func New() Heap {
	return &Binomial{
		listHead:      nil,
		elementsCount: 0,
	}
}

// Add adds a new value into the heap.
func (h *Binomial) Add(val Key) {
	if val == nil {
		return
	}

	h.insertNode(&Node{value: val, elementsCount: 1})
}

// Push adds a new value into the heap.
func (h *Binomial) Push(val ...Key) {
	for _, v := range val {
		if val != nil {
			h.insertNode(&Node{value: v, elementsCount: 1})
		}
	}
}

func (h *Binomial) insertNode(node *Node) {
	if h.listHead == nil {
		h.listHead = node
		h.elementsCount = node.elementsCount

		return
	}

	similar := findElementsCount(h.listHead, node.elementsCount)

	if similar == nil {
		h.listHead = addToList(h.listHead, node)
		h.elementsCount += node.elementsCount

		return
	}

	h.elementsCount -= similar.elementsCount
	h.listHead = removeFromList(h.listHead, similar)

	similar.sibling = nil

	merged := merge(similar, node)

	h.insertNode(merged)
}

// Pop removes the max element from this max heap.
func (h *Binomial) Pop() Key {
	if h == nil || h.listHead == nil {
		return nil
	}

	max, current := h.listHead, h.listHead
	for current != nil {
		if max.value.Less(current.value) {
			max = current
		}

		current = current.sibling
	}

	h.listHead = removeFromList(h.listHead, max)
	h.elementsCount -= max.elementsCount

	child := max.leftmostChild
	for child != nil {
		next := child.sibling
		child.sibling = nil

		h.insertNode(child)

		child = next
	}

	return max.value
}

// Clear removes all elements from this heap.
func (h *Binomial) Clear() {
	h.listHead = nil
	h.elementsCount = 0
}

// String returns a string representation of container.
func (h *Binomial) String() string {
	values := make([]string, 0, h.elementsCount)

	Traverse(h.listHead, func(n *Node, level int) {
		if n.value != nil {
			values = append(values, fmt.Sprintf("(%d, %s)", level, n.value))
		}
	}, 0)

	return "BinomialHeap:\n" + strings.Join(values, ", ")
}

// Values returns all elements in this heap.
func (h *Binomial) Values() []Key {
	result := make([]Key, 0, h.elementsCount)

	Traverse(h.listHead, func(n *Node, level int) {
		result = append(result, n.value)
	}, 0)

	return result
}

// Size returns the elements count.
func (h *Binomial) Size() int {
	return h.elementsCount
}

// Peek returns the biggest element without removing it, (nil, false) if heap is empty.
func (h *Binomial) Peek() Key {
	if h == nil || h.listHead == nil || h.listHead.elementsCount == 0 {
		return nil
	}

	max, current := h.listHead, h.listHead
	for current != nil {
		if max.value.Less(current.value) {
			max = current
		}

		current = current.sibling
	}

	return max.value
}
