package binaryheap

import (
	"fmt"
	"strings"
)

// Binary is implemented as a binary tree.
type Binary struct {
	root          *Node
	elementsCount int
}

// New instantiates a new empty heap tree.
func New() Heap {
	return &Binary{root: nil, elementsCount: 0}
}

// Add adds a value onto the heap and bubbles it up accordingly.
func (h *Binary) Add(val Key) {
	if val == nil {
		return
	}

	newNode := Node{value: val, left: nil, right: nil, parent: nil}

	if h.elementsCount == 0 {
		h.root = &newNode
		h.elementsCount = 1

		return
	}

	parent := h.findNode((h.elementsCount - 1) >> 1)
	if parent.left == nil {
		parent.left = &newNode
	} else {
		parent.right = &newNode
	}

	newNode.parent = parent
	newNode.bubbleUp()

	h.elementsCount++
}

// Push adds a value onto the heap and bubbles it up accordingly.
func (h *Binary) Push(values ...Key) {
	for i := range values {
		h.Add(values[i])
	}
}

// Pop removes top element on heap and returns it, or nil if heap is empty.
func (h *Binary) Pop() Key {
	if h.elementsCount == 0 {
		return nil
	}

	if h.elementsCount == 1 {
		result := h.root.value

		h.Clear()

		return result
	}

	h.elementsCount--
	result := h.root.value

	if h.root.left == nil {
		h.root = h.root.right
		h.root.parent = nil

		return result
	}

	if h.root.right == nil {
		h.root = h.root.left
		h.root.parent = nil

		return result
	}

	leaf := h.findNode(h.elementsCount)
	if leaf.parent.left == leaf {
		leaf.parent.left = nil
	} else {
		leaf.parent.right = nil
	}

	leaf.parent = nil

	h.root.value = leaf.value
	h.root.bubbleDown()

	return result
}

func (h *Binary) findNode(index int) *Node {
	path := make([]bool, 0)

	for index > 0 {
		if index%2 == 1 {
			path = append(path, true)
		} else {
			path = append(path, false)
		}

		index = (index - 1) >> 1
	}

	result := h.root

	for i := len(path) - 1; i >= 0; i-- {
		if path[i] {
			result = result.left
		} else {
			result = result.right
		}
	}

	return result
}

// Clear removes all elements from the heap.
func (h *Binary) Clear() {
	h.root = nil
	h.elementsCount = 0
}

// String returns a string representation of container.
func (h *Binary) String() string {
	values := make([]string, 0, h.elementsCount)

	Traverse(h.root, func(n *Node, level int) {
		if n.value != nil {
			values = append(values, fmt.Sprintf("(%d, %s)", level, n.value))
		}
	}, 0)

	return "BinaryHeap:\n" + strings.Join(values, ", ")
}

// Values returns all elements in the heap.
func (h *Binary) Values() []Key {
	result := make([]Key, 0, h.elementsCount)

	Traverse(h.root, func(n *Node, level int) {
		result = append(result, n.value)
	}, 0)

	return result
}

// Size returns number of elements within the heap.
func (h *Binary) Size() int {
	return h.elementsCount
}

// Peek returns the biggest element without removing it, (nil, false) if heap is empty.
func (h *Binary) Peek() Key {
	if h.elementsCount == 0 {
		return nil
	}

	return h.root.value
}
