package binaryheap

import (
	"fmt"
	"strings"
)

// Binary implements the Heap interface.
type Binary struct {
	list []Key
}

// New instantiates a new empty heap tree.
func New() Heap {
	return &Binary{list: []Key{}}
}

// Add adds a value onto the heap and bubbles it up accordingly.
func (h *Binary) Add(val Key) {
	if val == nil {
		return
	}

	h.list = append(h.list, val)

	h.bubbleUp()
}

// Parent returns the parent node's index.
func (h *Binary) Parent(index int) int {
	if index <= 0 {
		return 0
	}

	return (index - 1) / 2
}

// Left returns the left child's index.
func (h *Binary) Left(index int) int {
	if index < 0 {
		return 0
	}

	return 2*index + 1
}

// Right returns the right child's index.
func (h *Binary) Right(index int) int {
	if index < 0 {
		return 0
	}

	return 2*index + 2
}

// Push adds values onto our heap then restored the heap property.
func (h *Binary) Push(values ...Key) {
	if len(values) == 1 {
		h.Add(values[0])

		return
	}

	filtered := h.filterNils(values)
	h.list = append(h.list, filtered...)

	for i := 1 + len(h.list)/2; i >= 0; i-- {
		h.bubbleDownIndex(i)
	}
}

func (h *Binary) filterNils(input []Key) []Key {
	result := make([]Key, 0, len(input))

	for i := 0; i < len(input); i++ {
		if input[i] != nil {
			result = append(result, input[i])
		}
	}

	return result
}

// Pop removes top element on heap and returns it, or nil if heap is empty.
// Second return parameter is true, unless the heap was empty and there was nothing to pop.
func (h *Binary) Pop() Key {
	if len(h.list) == 0 {
		return nil
	}

	result := h.list[0]
	lastIndex := len(h.list) - 1

	h.list[0], h.list[lastIndex] = h.list[lastIndex], h.list[0]
	h.list = h.list[0:lastIndex]

	h.bubbleDown()

	return result
}

// Clear removes all elements from the heap.
func (h *Binary) Clear() {
	h.list = []Key{}
}

// String returns a string representation of container.
func (h *Binary) String() string {
	values := make([]string, 0, h.Size())

	for _, value := range h.list {
		values = append(values, fmt.Sprintf("%s", value))
	}

	return "BinaryHeap:\n" + strings.Join(values, ", ")
}

// Performs the "bubble down" operation. This is to place the element that is at the root
// of the heap in its correct place so that the heap maintains the min/max-heap order property.
func (h *Binary) bubbleDown() {
	h.bubbleDownIndex(0)
}

// Performs the "bubble down" operation. This is to place the element that is at the index
// of the heap in its correct place so that the heap maintains the min/max-heap order property.
func (h *Binary) bubbleDownIndex(index int) {
	for left := h.Left(index); left < len(h.list); left = h.Left(index) {
		right := h.Right(index)
		largestChild := left

		if right < len(h.list) && h.list[left].Less(h.list[right]) {
			largestChild = right
		}

		if h.list[index].Less(h.list[largestChild]) {
			h.list[index], h.list[largestChild] =
				h.list[largestChild], h.list[index]
		} else {
			break
		}

		index = largestChild
	}
}

// Performs the "bubble up" operation. This is to place a newly inserted
// element (i.e. last element in the list) in its correct place so that
// the heap maintains the min/max-heap order property.
func (h *Binary) bubbleUp() {
	index := len(h.list) - 1

	for parentIndex := h.Parent(index); index > 0; parentIndex = h.Parent(index) {
		indexValue := h.list[index]
		parentValue := h.list[parentIndex]

		if indexValue.Less(parentValue) {
			break
		}

		h.list[index], h.list[parentIndex] = h.list[parentIndex], h.list[index]
		index = parentIndex
	}
}

// Values returns all elements in the heap.
func (h *Binary) Values() []Key {
	return h.list
}

// Size returns number of elements within this heap.
func (h *Binary) Size() int {
	return len(h.list)
}

// Peek returns the biggest element without removing it, (nil, false) if heap is empty.
func (h *Binary) Peek() Key {
	if len(h.list) == 0 {
		return nil
	}

	return h.list[0]
}
