package binaryheap_test

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	binaryheap "gitlab.com/telo_tade/heaps/binary/slice"
)

type IntKey int

func (i IntKey) Less(k binaryheap.Key) bool {
	return i < k.(IntKey)
}

func (i IntKey) Eq(k binaryheap.Key) bool {
	return i == k.(IntKey)
}

func (i IntKey) String() string {
	return strconv.Itoa(int(i))
}

func TestBinaryHeapPush(t *testing.T) {
	heap := binaryheap.New()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3))
	heap.Push(IntKey(2))
	heap.Push(IntKey(1))

	assert.Equal(t, "[3 2 1]", fmt.Sprintf("%v", heap.Values()),
		"Got %v expected %v", fmt.Sprintf("%v", heap.Values()), "[3 2 1]")

	assert.Equal(t, 3, heap.Size(), "heap should have 3 values")

	actualValue := heap.Peek()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.True(t, actualValue.Eq(IntKey(3)), "Got %v expected %v", actualValue, 3)
}

func TestBinaryHeapPushBulk(t *testing.T) {
	heap := binaryheap.New()

	heap.Push(IntKey(15), IntKey(20), IntKey(3), IntKey(1), IntKey(2))

	assert.Equal(t, "[20 15 3 1 2]", fmt.Sprintf("%v", heap.Values()),
		"Got %v expected %v", fmt.Sprintf("%v", heap.Values()), "[20 15 3 1 2]")

	actualValue := heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(20), actualValue, "actualValue should be 20")
}

func TestBinaryHeapPop(t *testing.T) {
	heap := binaryheap.New()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3))
	heap.Push(IntKey(2))
	heap.Push(IntKey(1))
	heap.Pop()

	actualValue := heap.Peek()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(2), actualValue, "actualValue should be 2")

	actualValue = heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(2), actualValue, "actualValue should be 2")

	actualValue = heap.Pop()
	assert.NotNil(t, actualValue, "actualValue should not be nil")
	assert.Equal(t, IntKey(1), actualValue, "actualValue should be 1")

	actualValue = heap.Pop()
	assert.Nil(t, actualValue, "actualValue should be nil")
	assert.Nil(t, actualValue, "actualValue should be nil")

	assert.Equal(t, 0, heap.Size(), "heap should be empty")
	assert.Equal(t, 0, len(heap.Values()), "len(heap.Values()) should be 0")
}

func TestBinaryHeapRandom(t *testing.T) {
	heap := binaryheap.New()

	for i := 0; i < 1000; i++ {
		r, _ := rand.Int(rand.Reader, big.NewInt(30))
		heap.Push(IntKey(r.Int64()))
	}

	prev := heap.Pop()
	assert.NotNil(t, prev, "prev should not be nil")

	for heap.Size() != 0 {
		curr := heap.Pop()
		assert.NotNil(t, curr, "curr should not be nil")

		assert.False(t, prev.Less(curr),
			"Heap property invalidated. prev: %v current: %v", prev, curr)

		prev = curr
	}
}

func TestParent(t *testing.T) {
	heap := binaryheap.New()

	assert.Equal(t, 0, heap.Parent(-5), "Wrong parent value")
	assert.Equal(t, 8, heap.Parent(18), "Wrong parent value")
	assert.Equal(t, 9, heap.Parent(19), "Wrong parent value")
}

func TestLeft(t *testing.T) {
	heap := binaryheap.New()

	assert.Equal(t, 0, heap.Left(-5), "Wrong left value")
	assert.Equal(t, 11, heap.Left(5), "Wrong left value")
	assert.Equal(t, 13, heap.Left(6), "Wrong left value")
}

func TestRight(t *testing.T) {
	heap := binaryheap.New()

	assert.Equal(t, 0, heap.Right(-5), "Wrong right value")
	assert.Equal(t, 18, heap.Right(8), "Wrong right value")
	assert.Equal(t, 20, heap.Right(9), "Wrong right value")
}

func TestString(t *testing.T) {
	heap := binaryheap.New()

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	actual := heap.String()
	expected := "BinaryHeap:\n21, 18, 14, 7, 3"

	assert.Equal(t, expected, actual)
}

func TestValues(t *testing.T) {
	heap := binaryheap.New()

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	actual := fmt.Sprintf("%v", heap.Values())
	expected := "[21 18 14 7 3]"

	assert.Equal(t, expected, actual)
}

func TestSize(t *testing.T) {
	heap := binaryheap.New()
	assert.Equal(t, 0, heap.Size())

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	assert.Equal(t, 5, heap.Size())
}

func TestPeek(t *testing.T) {
	heap := binaryheap.New()

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))

	actual := heap.Peek()
	assert.Equal(t, IntKey(21), actual)
}

func TestClear(t *testing.T) {
	heap := binaryheap.New()
	heap.Clear()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))
	assert.Equal(t, "[21 18 14 7 3]", fmt.Sprintf("%v", heap.Values()))

	heap.Clear()
	assert.Equal(t, 0, heap.Size(), "heap should be empty")

	heap.Push(IntKey(3), IntKey(21), IntKey(14), IntKey(7), IntKey(18))
	assert.Equal(t, "[21 18 14 7 3]", fmt.Sprintf("%v", heap.Values()))
}

func randomPushPop() {
	heap := binaryheap.New()

	for i := 0; i < 1000; i++ {
		r, _ := rand.Int(rand.Reader, big.NewInt(100))

		heap.Push(IntKey(r.Int64()))
	}

	for heap.Size() != 0 {
		heap.Pop()
	}
}

func Benchmark_randomPushPop(b *testing.B) {
	for i := 0; i < b.N; i++ {
		randomPushPop()
	}
}
